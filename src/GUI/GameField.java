package GUI;

import Game.GameLogic;
import Game.GameMap;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;

import static Game.GameLogic.*;


public class GameField extends JPanel{
    private int width;
    private int height;
    private double cellWidth;
    private double cellHeight;

    private GameMap map;

    public GameField(GameLogic gameLogic) {
        map = gameLogic.map;
        setBackground(Color.GRAY);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                int clX = (int) (e.getX() / cellWidth);
                int clY = (int) (e.getY() / cellHeight);
                System.out.println(clX + " " + clY);
                if (gameRunning) {
                    gameLogic.turn(clX, clY);
                    repaint();
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

        width = getWidth();
        height = getHeight();
        g2.setColor(Color.GRAY);
        g2.fillRect(0,0, width, height);

        cellWidth = (double)width/map.MAP_SIZE;
        cellHeight = (double)height/map.MAP_SIZE;

        double per = 0.15d; // 15%
        int indent = (map.MAP_SIZE < 5) ? (int)(cellWidth*per) : 0;
        //int depthLine = (int)(cellWidth/2*per);
        g2.setColor(Color.WHITE);

        for (int i = 1; i < map.MAP_SIZE; i++) {
            g2.draw(new Line2D.Double(i*cellWidth, indent, i*cellWidth, height-indent));
            g2.draw(new Line2D.Double(indent, i*cellHeight, width-indent, i*cellHeight));
        }
        for (int i = 0; i < map.MAP_SIZE; i++) {
            for (int j = 0; j < map.MAP_SIZE; j++) {
                if (map.curMap[j][i] == DOT_O) {
                    g2.setColor(Color.WHITE);
                    g2.draw(new Ellipse2D.Double(i*cellWidth+indent, j*cellHeight+indent, cellWidth-indent*2, cellHeight-indent*2));
                }
                else if (map.curMap[j][i] == DOT_X) {
                    g2.setColor(Color.BLACK);
                    //g2.setStroke(new BasicStroke());
                    g2.draw(new Line2D.Double(i*cellWidth+indent, j*cellHeight+indent, i*cellWidth+(cellWidth-indent), j*cellHeight+(cellHeight-indent)));
                    g2.draw(new Line2D.Double(i*cellWidth+(cellWidth-indent), j*cellHeight+indent, i*cellWidth+indent, j*cellHeight+(cellHeight-indent)));
                }
            }
        }
        if (gameWin) {
            int[] coords = map.getCoordsWin();
            g2.setColor(Color.GREEN);
            g2.setStroke(new BasicStroke(2));
            g2.draw(new Line2D.Double(
                    coords[0]*cellWidth+(cellWidth/2),
                    coords[1]*cellHeight+(cellHeight/2),
                    coords[2]*cellWidth+(cellWidth/2),
                    coords[3]*cellHeight+(cellHeight/2))
            );
            ScoreBar.updateScore();
        }
    }

}
