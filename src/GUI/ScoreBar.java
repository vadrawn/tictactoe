package GUI;

import javax.swing.*;
import java.awt.*;

import static Game.GameLogic.scoreAi;
import static Game.GameLogic.scorePlayer;


class ScoreBar extends JPanel{
    private static JLabel jScoreX = new JLabel();
    private static JLabel jScoreY = new JLabel();

    ScoreBar(){
        setLayout(new FlowLayout());

        setBorder(BorderFactory.createLineBorder(Color.darkGray));
        updateScore();

        // Add
        add(jScoreX);
        add(jScoreY);
    }

    static void updateScore() {
        jScoreX.setText("Игрок: " + scorePlayer);
        jScoreY.setText("Компьютер: " + scoreAi);
    }
}
