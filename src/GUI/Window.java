package GUI;


import Game.GameLogic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;


public class Window extends JFrame {

    public Window(GameLogic gameLogic) {
        // Init
        setTitle("Tic-Tac-Toe");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(400, 500);
        setLocationRelativeTo(null); // окно по центру экрана
        //setResizable(false);

        // ----- Up Panel for score
        ScoreBar scoreBar = new ScoreBar();

        // ----- Game Field (Panel) -----
        GameField gameField = new GameField(gameLogic);

        // ----- Bottom Panel for buttons -----
        JPanel jBottomPanel = new JPanel();
        jBottomPanel.setLayout(new FlowLayout());
        JButton jButtonNG1 = new JButton("Новая игра");
        //JButton jButtonNG2 = new JButton("Два игрока");
        JButton jButtonExit = new JButton("Выход");
        jBottomPanel.add(jButtonNG1);
        //jBottomPanel.add(jButtonNG2);
        jBottomPanel.add(jButtonExit);
        // ========== Actions for buttons ===========
        // New Game button
        jButtonNG1.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int side = getSide("За кого будете играть?");
                if (side != -1) {
                    gameLogic.startGame(side);
                    repaint();
                }
            }
        });
        /*
        jButtonNG2.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int side = getSide("Игрок 1, выберите сторону");
                int mode = 1;
                if (side != -1) {
                    gameLogic.startGame(side, mode);
                    repaint();
                }
            }
        });*/
        // Exit button
        jButtonExit.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        // Add elements
        add(scoreBar, BorderLayout.NORTH);
        add(gameField, BorderLayout.CENTER);
        add(jBottomPanel, BorderLayout.SOUTH);

        setVisible(true);
    }

    private int getSide(String question) {
        Object[] options = {"X", "O"};
        int side = JOptionPane.showOptionDialog(
                Window.this,
                question,
                "Выбор стороны",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0] // по умолчанию
        );
        return side;
    }


}
