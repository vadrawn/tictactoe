package Game;


import static Game.GameLogic.DOT_EMPTY;
import static Game.GameLogic.DOT_O;
import static Game.GameLogic.DOT_X;

public class Ai{
    char dot = DOT_O;
    private char dotInv = DOT_X;

    private int nextWinX;
    private int nextWinY;
    private int turnCnt = 0;

    Ai(char dot) {
        this.dot = dot;
        if (dot == DOT_O) this.dotInv = DOT_X;
        else this.dotInv = DOT_O;

    }

    void makeTurn(GameMap map){
        int x,y;
        int centerCell = map.MAP_SIZE/2;
        do {
            x = (int) (Math.random() * map.MAP_SIZE);
            y = (int) (Math.random() * map.MAP_SIZE);
        } while (!map.isCellValid(x, y));

        // Проверка на победу следующим ходом
        if (checkNextTurnWin(map, this.dot)){
            x = nextWinX;
            y = nextWinY;
        }
        // Проверка на победу противника
        else if (checkNextTurnWin(map, this.dotInv)) {
            x = nextWinX;
            y = nextWinY;
        }
        // Занять центр
        else if (map.curMap[centerCell][centerCell] == DOT_EMPTY) {
            x = centerCell;
            y = centerCell;
        }
        else if (map.curMap[centerCell][centerCell] == this.dotInv && this.turnCnt == 0) {
            x = centerCell-1;
            y = centerCell-1;
        }

        System.out.println("Компьютер сделал ход в точку " + (x+1) + " " + (y+1));
        map.curMap[y][x] = this.dot;
        this.turnCnt++;
    }

    private boolean checkNextTurnWin(GameMap map, char symb){
        char[][] next_map = new char[map.MAP_SIZE][map.MAP_SIZE];
        for (int x_ = 0; x_ < map.MAP_SIZE; x_++) {
            for (int y_ = 0; y_ < map.MAP_SIZE; y_++) {
                if (map.isCellValid(x_, y_)) {
                    System.arraycopy(map.curMap,0,next_map,0,map.MAP_SIZE);
                    next_map[y_][x_] = symb;
                    if (map.checkWin(next_map, symb)) {
                        nextWinX = x_;
                        nextWinY = y_;
                        return true;
                    }
                    next_map[y_][x_] = DOT_EMPTY;
                }
            }
        }
        return false;
    }
}
