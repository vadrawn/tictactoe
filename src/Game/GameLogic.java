package Game;


public class GameLogic {
    public static final char DOT_EMPTY = '*';
    public static final char DOT_X = 'X';
    public static final char DOT_O = 'O';

    public static boolean gameRunning = false;
    public static boolean gameWin = false;

    public static int scorePlayer = 0;
    public static int scoreAi = 0;

    public GameMap map;
    private Player player;
    private Ai ai;

    public GameLogic(GameMap map){
        this.map = map;
    }

    public void startGame(int side){
        selectSide(side);
        map.initMap();
        gameWin = false;
        gameRunning = true;
        if (ai.dot == DOT_X)
            ai.makeTurn(map);
    }

    public void turn(int x, int y) {
        if (player.makeTurn(map, x, y)) {
            if (map.checkWin(map.curMap, player.dot))
                printWin(player.dot);
            if (gameRunning && map.isMapFull())
                printWin('D');
            if (gameRunning) {
                ai.makeTurn(map);
                if (map.checkWin(map.curMap, ai.dot)) printWin(ai.dot);
                if (gameRunning && map.isMapFull()) printWin('D');
            }
        }
    }

    private void printWin(char symb) {
        if (symb == player.dot) {
            scorePlayer++;
            gameWin = true;
            System.out.println("Победил человек!");
        }
        else if (symb == ai.dot){
            scoreAi++;
            gameWin = true;
            System.out.println("Победил Компьютер!");
        }
        else
            System.out.println("Ничья");

        System.out.println("Score: " + scorePlayer + " : " + scoreAi);
        gameRunning = false;
    }


    // ========== Init methods ========== //
    private void selectSide(int side){
        if (side == 0) {
            player = new Player(DOT_X);
            ai = new Ai(DOT_O);
        } else if (side == 1) {
            player = new Player(DOT_O);
            ai = new Ai(DOT_X);
        }
        System.out.println("Player: " + player.dot + ", ai: " + ai.dot);
    }


}
