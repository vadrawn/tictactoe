package Game;


import static Game.GameLogic.DOT_EMPTY;

public class GameMap {
    // General
    public final int MAP_SIZE;
    private final int DOTS_TO_WIN;
    public char[][] curMap;

    // Coordinates for Win
    private int[] coordsWin = new int[4];
    private int[] rowCoords = new int[4];
    private int[] columnCoords = new int[4];
    private int[] diag1Coords = new int[4];
    private int[] diag2Coords = new int[4];

    GameMap(int size){
       this.MAP_SIZE = size;
       if (size < 5) this.DOTS_TO_WIN = 3;
       else this.DOTS_TO_WIN = 5;
       initMap();
    }

    GameMap(){
       this(3);
    }

    void initMap() {
        curMap = new char[MAP_SIZE][MAP_SIZE];
        for (int i = 0; i < MAP_SIZE; i++) {
            for (int j = 0; j < MAP_SIZE; j++)
                curMap[i][j] = DOT_EMPTY;
        }
    }

    boolean isCellValid(int x, int y){
        if(x < 0 || x >= MAP_SIZE || y < 0 || y >= MAP_SIZE) return false;
        if (curMap[y][x] == DOT_EMPTY) return true;
        else return false;
    }

    boolean isMapFull(){
        for(int i=0;i<MAP_SIZE;i++){
            for(int j=0;j<MAP_SIZE;j++){
                if(curMap[i][j] == DOT_EMPTY) return false;
            }
        }
        return true;
    }

    boolean checkWin(char[][] map, char symb) {
        int diag1 = 0;
        int diag2 = 0;
        for (int i = 0; i < MAP_SIZE; i++) {
            int row = 0;
            int column = 0;
            for (int j = 0; j < MAP_SIZE; j++) {
                // Проверка горизонтали
                if(map[i][j] == symb) {
                    if (row == 0) winCoordsStart (j, i, 0);
                    row++;
                    if (row >= DOTS_TO_WIN) {
                        winCoordsEnd (j, i, 0);
                        winLine(0);
                        return true;
                    }
                }
                else if (map[i][j] != symb) row = 0;

                // Проверка вертикали
                if(map[j][i] == symb) {
                    if (column == 0) winCoordsStart (i, j, 1);
                    column++;
                    if (column >= DOTS_TO_WIN) {
                        winCoordsEnd (i, j, 1);
                        winLine(1);
                        return true;
                    }
                }
                else if(map[j][i] != symb) column = 0;

                // Проверка прямой диагонали
                if(i == j && map[i][j] == symb) {
                    if (diag1 == 0) winCoordsStart (i, j, 2);
                    diag1++;
                    if (diag1 >= DOTS_TO_WIN) {
                        winCoordsEnd (i, j, 2);
                        winLine(2);
                        return true;
                    }
                }
                else if (i == j && map[i][j] != symb) diag1 = 0;

                // Проверка обратной диагонали
                if(i+j == MAP_SIZE-1 && map[i][j] == symb) {
                    if (diag2 == 0) winCoordsStart (j, i, 3);
                    diag2++;
                    if (diag2 >= DOTS_TO_WIN) {
                        winCoordsEnd (j, i, 3);
                        winLine(3);
                        return true;
                    }
                }
                else if (i+j == MAP_SIZE-1 && map[i][j] != symb) diag2 = 0;
                //if(row >= DOTS_TO_WIN || column >= DOTS_TO_WIN || diag1 >= DOTS_TO_WIN || diag2 >= DOTS_TO_WIN) return true;
            }
        }
        return false;
    }

    private void winLine(int dir){
        switch (dir) {
            case 0: this.coordsWin = rowCoords;
                break;
            case 1: this.coordsWin = columnCoords;
                break;
            case 2: this.coordsWin = diag1Coords;
                break;
            case 3: this.coordsWin = diag2Coords;
                break;
            default: this.coordsWin = rowCoords;
                break;
        }
    }

    public int[] getCoordsWin() {
        return coordsWin;
    }

    private void winCoordsStart(int x1, int y1, int dir){
        if (dir == 0) {
            this.rowCoords[0] = x1;
            this.rowCoords[1] = y1;
        } // row
        else if (dir == 1) {
            this.columnCoords[0] = x1;
            this.columnCoords[1] = y1;
        } // column
        else if (dir == 2) {
            this.diag1Coords[0] = x1;
            this.diag1Coords[1] = y1;
        } // diagonal 1
        else if (dir == 3) {
            this.diag2Coords[0] = x1;
            this.diag2Coords[1] = y1;
        } // diagonal 2
    }
    private void winCoordsEnd(int x2, int y2, int dir){
        if (dir == 0) {
            this.rowCoords[2] = x2;
            this.rowCoords[3] = y2;
        } // row
        else if (dir == 1) {
            this.columnCoords[2] = x2;
            this.columnCoords[3] = y2;
        } // column
        else if (dir == 2) {
            this.diag1Coords[2] = x2;
            this.diag1Coords[3] = y2;
        } // diagonal 1
        else if (dir == 3) {
            this.diag2Coords[2] = x2;
            this.diag2Coords[3] = y2;
        } // diagonal 2
    }
}
