package Game;

import static Game.GameLogic.DOT_X;

public class Player{
    char dot = DOT_X;

    Player(char dot) {
        this.dot = dot;
    }

    boolean makeTurn(GameMap map, int x, int y) {
        if (map.isCellValid(x,y)) {
            map.curMap[y][x] = this.dot;
            return true;
        }
        else return false;
    }
}
