package Game;

import GUI.Window;

public class TicTacToe {
    public static void main(String[] args) {
        GameMap map = new GameMap();
        GameLogic gameLogic = new GameLogic(map);
        Window window = new Window(gameLogic);

    }
}
